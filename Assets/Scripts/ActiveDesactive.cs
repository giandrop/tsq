﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveDesactive : MonoBehaviour
{
    public GameObject Prender;
    public GameObject Apagar;
    public GameObject ParaActivar;
    // Start is called before the first frame update
    public void CambioDeCamara()
    {
        Prender.SetActive(true);
        Apagar.SetActive(false);
    }

    public void Activar()
    {
        ParaActivar.SetActive(true);

    }
}
