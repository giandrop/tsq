﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform followTransform;
    

    // Update is called once per frameddd
    void Update()
    {
        //transform.position = new Vector3(followTransform.position.x + 4.2f, 0f, transform.position.z);
        transform.position = new Vector3(followTransform.position.x + 4.2f, (followTransform.position.y + 6) * 0.6f, transform.position.z);
    }
}