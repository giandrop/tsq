﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [Header("Menu")]
    public Animator Camara;
    public Animator PanelInicio;
    public Animator Botons;

    [Header("Musica")]
    public AudioSource MusicaMenu;
    public AudioClip Menu;

    [Header("SFX")]
    public AudioSource SFX;
    public AudioClip Clic;

    [Header("GamePlay")]
    public GameObject Player;
    public GameObject Objetos;

    void Start()
    {
        Time.timeScale = 1f;
        
    }
    public void MusicMenu()
    {
        MusicaMenu.clip = Menu;
        MusicaMenu.Play();
    }
    
    public void Play()
    {
        Player.SetActive(true);
        Objetos.SetActive(true);
        SFX.clip = Clic;
        SFX.Play();
        PanelInicio.SetBool("Play", true);
        Botons.SetBool("Play", true);
        Camara.SetBool("Play", true);
        MusicaMenu.volume = 0f;
    }
}
