﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    
    private Vector3 lastCameraPosition;
    
    public Transform cameraTransform;
    public float parallaxEffectMultiplierHorizontally = 0.5f;
    public float parallaxEffectMultiplierVertically = 0.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        lastCameraPosition = cameraTransform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 deltaMovement = cameraTransform.position - lastCameraPosition;
        Vector3 movement = new Vector3(deltaMovement.x * parallaxEffectMultiplierHorizontally, deltaMovement.y * parallaxEffectMultiplierVertically, 0);
        transform.position += movement;
        lastCameraPosition = cameraTransform.position;
    }
}
