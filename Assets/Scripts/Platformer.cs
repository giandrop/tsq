﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platformer : MonoBehaviour
{
    [Header("Variables")]
    public float speed;
    public float jumpForce;
    bool isGrounded = false; 

    [Header("GO")]
    public LayerMask groundLayer;
    
    private AudioSource SFX;

    [Header("SFX")]
    public AudioClip Caminar;
    public AudioSource Saltar;
    public AudioSource Chocar;
    public AudioSource TomarObjetos;

    [Header("Animaciones")]
    public GameObject EstaticoDerecha;
    public GameObject EstaticoIzquiera;
    public GameObject CorrerDerecha;
    public GameObject CorrerIzquiera;
    public GameObject SaltarDerecha;
    public GameObject SaltarIzquiera;
    public Animator Player; 
    
    Rigidbody2D rb;
    CapsuleCollider2D bbox;

    void Start()
    {
        SFX = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();
        bbox = GetComponent<CapsuleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
        CheckIfGrounded();

        if(Input.GetKeyDown("space"))
        {
            Saltar.Play();
        }
        if((Input.GetAxisRaw("Horizontal") <= 0) && (isGrounded == false))
        {
            EstaticoDerecha.SetActive(false);
            EstaticoIzquiera.SetActive(false);
            SaltarIzquiera.SetActive(true);
            SaltarDerecha.SetActive(false);
            CorrerDerecha.SetActive(false);
            CorrerIzquiera.SetActive(false);
        }
        if((Input.GetAxisRaw("Horizontal") >= 0) && (isGrounded == false))
        {
            EstaticoDerecha.SetActive(false);
            EstaticoIzquiera.SetActive(false);
            SaltarIzquiera.SetActive(false);
            SaltarDerecha.SetActive(true);
            CorrerDerecha.SetActive(false);
            CorrerIzquiera.SetActive(false);
        }  
        if(isGrounded == true)
        {
            SaltarDerecha.SetActive(false);
            SaltarIzquiera.SetActive(false);
        } 
    }
    public void DesactivarAnimator()
    {
        Player.enabled = false;
    }

    void OnCollisionStay2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Piso" && Input.GetAxisRaw("Horizontal") != 0f)
        {
            SFX.clip = Caminar;
            SFX.enabled = true; 
        }
        else
        {
            SFX.enabled = false; 
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "zapas")
        {
            TomarObjetos.Play();
        }
    }

    void Move() 
    {
        float x = Input.GetAxisRaw("Horizontal");
        float moveBy = x * speed;
        rb.velocity = new Vector2(moveBy, rb.velocity.y);

        if(Input.GetAxisRaw("Horizontal") > 0)
        {
            EstaticoDerecha.SetActive(false);
            EstaticoIzquiera.SetActive(false);
            CorrerDerecha.SetActive(true);
            CorrerIzquiera.SetActive(false);
        }
        if(Input.GetAxisRaw("Horizontal") < 0)
        {
            EstaticoDerecha.SetActive(false);
            EstaticoIzquiera.SetActive(false);
            CorrerDerecha.SetActive(false);
            CorrerIzquiera.SetActive(true);
        }
        if(Input.GetAxisRaw("Horizontal") == 0)
        {
            EstaticoDerecha.SetActive(true);
            EstaticoIzquiera.SetActive(false);
            CorrerDerecha.SetActive(false);
            CorrerIzquiera.SetActive(false);
        }
    }

    void Jump() 
    { 
        if ((Input.GetKeyDown("v") || Input.GetKeyDown("space")) && isGrounded) 
        { 
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }
    }

    void CheckIfGrounded() 
    { 
        Vector2 position = new Vector2(bbox.bounds.center.x, bbox.bounds.center.y - bbox.bounds.extents.y/2 - 0.05f);
        Vector2 size = new Vector2(bbox.bounds.size.x - 0.05f, bbox.bounds.size.y / 2);
        
        Collider2D collider = Physics2D.OverlapBox(position, size, 0, groundLayer);

        /* Area de deteccion de suelo */
        Debug.DrawLine(new Vector3(position.x - size.x/2, position.y - size.y/2, 0), new Vector3(position.x + size.x/2, position.y - size.y/2, 0), Color.blue);
        Debug.DrawLine(new Vector3(position.x + size.x/2, position.y - size.y/2, 0), new Vector3(position.x + size.x/2, position.y + size.y/2, 0), Color.blue);
        Debug.DrawLine(new Vector3(position.x + size.x/2, position.y + size.y/2, 0), new Vector3(position.x - size.x/2, position.y + size.y/2, 0), Color.blue);
        Debug.DrawLine(new Vector3(position.x - size.x/2, position.y + size.y/2, 0), new Vector3(position.x - size.x/2, position.y - size.y/2, 0), Color.blue);

        if (collider != null) {
            isGrounded = true; 
        } else { 
            isGrounded = false; 
        } 
    }
}
