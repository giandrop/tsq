﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivaAnimacion : MonoBehaviour
{
    public Animator OBJ;
    public AudioSource OBJSound;
    void OnCollisionStay2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            OBJ.SetBool("Salto", true);
            OBJSound.Play();
        }
       
    }
    
}
