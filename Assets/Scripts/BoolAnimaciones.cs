﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoolAnimaciones : MonoBehaviour
{
    public Animator OBJ;

    public void Desactivar()
    {
        OBJ.SetBool("Salto", false);
    }
}
