﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GloboMovement : MonoBehaviour
{

    Rigidbody2D rb;
    Vector2 initialPosition;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        initialPosition = rb.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 vec = initialPosition - rb.position;
        vec.Normalize();
        rb.velocity = vec * 0.25f;
        rb.velocity += new Vector2(0, Mathf.Sin(Time.time));
    }
}
