﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Escenas")]
    public string EscenaInicial;
    public string EscenaActual;
    [Header("Pausa")]
    public GameObject PanelPausaDesk;
    public GameObject PanelPausaMovil;
    public static bool GameIsPaused = false;

    [Header("SFX")]
    public AudioSource SFX;
    public AudioClip Clic;
    public AudioSource Pause;

    [Header("Musica")]
    public AudioSource MusicaJuego;
    public AudioClip MusicaNivel;
    public Animator VolumenMusica;
 
    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Continuar();
            }
            else
            {
                if(Time.timeScale == 1f)
                {
                    Pausa();
                }
            }
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(EscenaActual);
        }
        if(Input.GetKeyDown(KeyCode.I))
        {
            SceneManager.LoadScene(EscenaInicial);
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            Application.Quit();
            Debug.Log("Chau");
        }
    }
    public void Continuar()
    {
        MusicaJuego.Play();
        PanelPausaDesk.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        Pause.Play();
    }
    public void Pausa()
    {
        MusicaJuego.Pause();
        PanelPausaDesk.SetActive(true);
        GameIsPaused = true;
        Pause.Play();
    }
    public void Reset()
    {
        GameIsPaused = false;
        SFX.clip = Clic;
        SFX.Play();
        SceneManager.LoadScene(0);
    }
    public void ActivarMusicaGame()
    {
        VolumenMusica.SetBool("Subir", true);
        MusicaJuego.clip = MusicaNivel;
        MusicaJuego.Play();
    }
}
