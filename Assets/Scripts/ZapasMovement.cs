﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZapasMovement : MonoBehaviour
{

    public float multiplicadorSalto = 2;
    public float distancia = 0.2f;

    Vector2 initialPosition;
    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = initialPosition + new Vector2(0, Mathf.Sin(Time.time) * distancia);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            Platformer player = other.gameObject.GetComponent<Platformer>();
            player.jumpForce = player.jumpForce * multiplicadorSalto;
            Debug.Log("Zapas");
            Destroy(this.gameObject);
        }
       
    }

}
