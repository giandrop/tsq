﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionTrigger : MonoBehaviour
{
    public Animator Objeto;

    public AudioSource OBJSound;

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            Objeto.SetBool("Salto", true);
            OBJSound.Play();
        }
    }
}
